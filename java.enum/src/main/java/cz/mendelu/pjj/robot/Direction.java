package cz.mendelu.pjj.robot;


import static cz.mendelu.pjj.robot.Coordinate.coordinate;
/**
 * Created by Honza on 09.11.2016.
 */
public enum Direction {
    NORTH(0, -1),
    NORTH_EAST(1, -1),
    EAST(1, 0),
    SOUTH_EAST(1, 1),
    SOUTH(0, 1),
    SOUTH_WEST(-1, 1),
    WEST(-1, 0),
    NORTH_WEST(-1, -1);

    private final int x, y;

    private static Direction[] VALUES = values();
    private static int LENGTH = values().length;

    Direction(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Direction onLeft() {
        int leftOrdinal = (LENGTH + ordinal() - 1) % LENGTH;
        return VALUES[leftOrdinal];
    }

    public Direction onRight() {
        int rightOrdinal = (ordinal() + 1) % LENGTH;
        return VALUES[rightOrdinal];
    }

    public Coordinate oneStepFrom(Coordinate from) {
        int dx = from.x + x;
        int dy = from.y + y;
        return coordinate(dx, dy);
    }
}
