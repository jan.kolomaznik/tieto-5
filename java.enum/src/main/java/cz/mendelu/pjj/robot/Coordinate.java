package cz.mendelu.pjj.robot;

import java.util.HashMap;
import java.util.Map;

public class Coordinate {

    private static int count;

    public final int x;
    public final int y;

    private static Map<String, Coordinate> cache = new HashMap<>();

    public static Coordinate coordinate(int x, int y) {
        String key = x + ":" + y;
        if (!cache.containsKey(key)) {
            Coordinate coordinate = new Coordinate(x, y);
            cache.put(key, coordinate);
            return coordinate;
        }

        return cache.get(key);
    }

    private Coordinate(int x, int y) {
        System.out.format("Coordinate created: %d%n", count++);
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coordinate that = (Coordinate) o;

        if (x != that.x) return false;
        return y == that.y;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        return result;
    }

    @Override
    public String toString() {
        return "[" + x + ", " + y + ']';
    }
}
