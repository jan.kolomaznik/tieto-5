package cz.mendelu.pjj.robot;

import java.util.HashMap;
import java.util.Map;

import static cz.mendelu.pjj.robot.Coordinate.coordinate;

/**
 * Created by Honza on 08.11.2016.
 */
public class World {

    private final int width;
    private final int height;
    private Map<Coordinate, Object> map;

    public World(int width, int height) {
        this.width = width;
        this.height = height;
        this.map = new HashMap<>();
    }

    public Object getTreasureAt(int x, int y){
        Coordinate coordinate = coordinate(x, y);
        return map.get(coordinate);
    }

    public void removeTreasureAt(int x, int y){
        Coordinate coordinate = coordinate(x, y);
        map.remove(coordinate);
    }

    public Object addTreasureAt(Object object, int x, int y){
        Coordinate coordinate = coordinate(x, y);
        map.put(coordinate, object);
        return object;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

}
