package cz.mendelu.pjj.robot;

import static cz.mendelu.pjj.robot.Coordinate.coordinate;
/**
 * Created by Honza on 08.11.2016.
 */
public class Robot {

    private Coordinate coordinate;
    private Direction direction;

    public Robot(int x, int y, Direction direction) {
        this.coordinate = coordinate(x, y);
        this.direction = direction;
    }

    public void forward() {
        coordinate = direction.oneStepFrom(coordinate);
    }

    public void turnLeft() {
        direction = direction.onLeft();
    }

    public void turnRight() {
        direction = direction.onRight();
    }

    public Coordinate getPosition() {
        return coordinate;
    }

    public String getDirectionName() {
        return direction.name().toLowerCase();
    }

}
