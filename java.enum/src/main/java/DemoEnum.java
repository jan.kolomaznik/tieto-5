public class DemoEnum {

    public enum DnyVTydnu {
        PO(),
        UT(),
        ST(),
        CT(),
        PA(),
        SE(false),
        NE(false) {
            @Override
            public DnyVTydnu nextDay() {
                return PO;
            }
        };

        private static final DnyVTydnu[] VALUES = values();

        private final boolean pracovniDen;

        DnyVTydnu() {
            this.pracovniDen = true;
        }

        DnyVTydnu(boolean pracovniDen) {
            this.pracovniDen = pracovniDen;
        }

        public boolean isPracovniDen() {
            return ordinal() < 5;
        }

        public DnyVTydnu nextDay() {
            return VALUES[ordinal()+1];
        }
    }

    public static abstract class DnyVTydnuClass {
        public static final DnyVTydnuClass PO = new DnyVTydnuClass(true, null) {
            @Override
            public DnyVTydnuClass nextDay() {
                return UT;
            }
        };
        public static final DnyVTydnuClass UT = new DnyVTydnuClass(true, PO) {
            @Override
            public DnyVTydnuClass nextDay() {
                return ST;
            }
        };
        public static final DnyVTydnuClass ST = new DnyVTydnuClass(true, UT) {
            @Override
            public DnyVTydnuClass nextDay() {
                return CT;
            }
        };
        public static final DnyVTydnuClass CT = new DnyVTydnuClass(true, ST) {
            @Override
            public DnyVTydnuClass nextDay() {
                return PA;
            }
        };
        public static final DnyVTydnuClass PA = new DnyVTydnuClass(true, CT) {
            @Override
            public DnyVTydnuClass nextDay() {
                return SO;
            }
        };
        public static final DnyVTydnuClass SO = new DnyVTydnuClass(false, PA) {
            @Override
            public DnyVTydnuClass nextDay() {
                return NE;
            }
        };
        public static final DnyVTydnuClass NE = new DnyVTydnuClass(false, SO) {
            @Override
            public DnyVTydnuClass nextDay() {
                return PO;
            }
        };

        private final boolean pracovniDen;

        private DnyVTydnuClass(boolean pracovniDen, DnyVTydnuClass beforeDay) {
            this.pracovniDen = pracovniDen;
            this.beforeDay = beforeDay;
        }

        public boolean isPracovniDen() {
            return pracovniDen;
        }

        private final DnyVTydnuClass beforeDay;

        public DnyVTydnuClass getBeforeDay() {
            return beforeDay;
        }

        public abstract DnyVTydnuClass nextDay();
    }

    public static void main(String[] args) {
        System.out.println(DnyVTydnuClass.CT.isPracovniDen());
        System.out.println(DnyVTydnu.CT.isPracovniDen());
    }
}
