package jm.java.streams.exercise;

import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Exercise: resolve the tasks in the code below.
 */
public final class CollectionBasics {

    /**
     * Runs the program.
     *
     * @param args
     *            command line arguments. It must not be {@code null}.
     */
    public static void main(String... args) {
        Random random = new Random();
        final List<Integer> numbers = random.ints(0, 100)
                .limit(10)
                .boxed()
                .collect(Collectors.toList()); // TODO: Make a list of random numbers
        // TODO: Remove all even numbers
        numbers.removeIf(n -> n % 2 == 0);
        // TODO: Sort the list
        numbers.sort(Comparator.comparingInt(a -> a));
        // TODO: Print the remaining numbers, one on each line
        numbers.forEach(n -> System.out.println(n));
        // TODO: Do it with lambda and with new methods on the collection
        System.out.println(numbers);

        random.ints(0, 100)
                .limit(10)
                .filter(n -> n % 2 == 0)
                .sorted()
                .forEach(System.out::println);
        // TODO: Rewrite all above with a single stream and no intermediate collection
    }
}
