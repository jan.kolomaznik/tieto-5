package jm.java.streams.lesson;

import java.util.Collections;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class SkipLimit {

    public static void main(String[] args) {
        System.out.println(IntStream.range(0, 10)
                .mapToObj(Integer::valueOf)
                .map(Objects::toString)
                .collect(Collectors.joining(":" , "<", ">")));

        Stream.of(1, 2, 3).map(i -> i++).forEach(System.out::println);

        System.out.println(Collections.emptySet()
                .stream()
                .findAny().orElseThrow(UnsupportedOperationException::new));

    }
}
