package cz.ictpro.java.design.patterns;

import java.util.ArrayList;

public class FactoryMetod {

    public interface Factory {
        String build();
    }

    static private Factory factory = "Ahoj"::toString;

    public static void main(String[] args) {
        String product = factory.build();
        System.out.print(product);


    }
}
