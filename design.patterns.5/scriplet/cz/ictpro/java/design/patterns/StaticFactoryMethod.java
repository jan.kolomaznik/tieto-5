package cz.ictpro.java.design.patterns;

public class StaticFactoryMethod {

    // Static factory method
    public static String build() {
        return "Ahoj";
    }

    public static void main(String[] args) {
        String product = StaticFactoryMethod.build();
        System.out.println(product);
    }


}
