package jm.java.generics.exercise;

import java.util.Objects;

public class Exercise {


    public static void main(String[] args) {
        // PUT api/nasedni/{carId = 1}/{zakaznikId = 2}
        Id<Auto> cadId = Auto.id(1);
        Id<Zakaznik> zakaznikId = new Id<>(Zakaznik.class, 1);

        String msg = nasedni(cadId, zakaznikId);
        System.out.println(msg);

        System.out.println(Objects.equals(cadId, zakaznikId));

    }

    public static String nasedni(Id<Auto> carId, Id<Zakaznik> zakaznikId) {
        Zakaznik z = Zakaznik.getZakaznik(zakaznikId);
        Auto a = Auto.getAuto(carId);
        return String.format("Zakaznik %s nasedl do auta %s.", z.getName(), a.getName());
    }
}
