package jm.java.generics.exercise;

import java.util.Objects;

/**
 * Object for ID
 */
public class Id<T> {

    private final long value;
    private final Class<T> type;

    Id(Class<T> type, long value) {
        this.value = value;
        this.type = type;
    }

    public long getValue() {
        return value;
    }

    public Class<T> getType() {
        return type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Id<?> id = (Id<?>) o;
        return value == id.value &&
                Objects.equals(type, id.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, type);
    }
}
