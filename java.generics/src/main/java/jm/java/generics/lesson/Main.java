package jm.java.generics.lesson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Integer> cislaI = Arrays.asList(1, 2, 3);
        List<Double> cislaD = Arrays.asList(1.0, 2d, 3d);
        System.out.println(suma(cislaI));
        System.out.println(suma(cislaD));
    }

    public static double suma(Collection<? extends Number> cisla) {
        double result = 0;
        for (Number e : cisla) {
            result += e.doubleValue();
        }
        return result;
    }

}
