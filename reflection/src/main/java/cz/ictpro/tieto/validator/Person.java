package cz.ictpro.tieto.validator;

public class Person {

    @NotNull(critical = true)
    private Long id;

    @NotNull
    private String name;

    private Integer year;

    public Person(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
