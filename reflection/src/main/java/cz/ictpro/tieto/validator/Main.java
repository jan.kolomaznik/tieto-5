package cz.ictpro.tieto.validator;

public class Main {

    public static void main(String[] args) {
        Person p1 = new Person(1L, "Pepa");
        Person p2 = new Person(2L, null);
        Person p3 = new Person(null, "Vasek");

        System.out.println(p1);
        Validator.validateNotNull(p1);

        System.out.println(p2);
        Validator.validateNotNull(p2);

        System.out.println(p3);
        Validator.validateNotNull(p3);
    }
}
