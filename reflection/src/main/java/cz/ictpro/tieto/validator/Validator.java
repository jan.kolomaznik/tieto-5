package cz.ictpro.tieto.validator;

import java.lang.reflect.Field;
import java.util.Arrays;

public class Validator {

    public static void validateNotNull(Object o) {
        Class c = o.getClass();
        Field[] fields = c.getDeclaredFields();

        for (Field field: fields) {
            NotNull notNull = field.getAnnotation(NotNull.class);
            if (notNull != null) {
                try {
                    field.setAccessible(true);
                    Object value = field.get(o);
                    if (value == null) {
                        if (notNull.critical()) {
                            throw new IllegalArgumentException(field.getName() + " is null.");
                        } else {
                            System.out.println(field.getName() + " is null.");
                        }
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }

            }
        }

    }
}
