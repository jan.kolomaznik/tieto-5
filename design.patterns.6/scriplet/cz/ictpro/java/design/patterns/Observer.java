package cz.ictpro.java.design.patterns;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Observer {

    public static class Point {

        private int x, y;

        private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

        public void addPropertyChangeListener(PropertyChangeListener listener) {
            pcs.addPropertyChangeListener(listener);
        }

        public void removePropertyChangeListener(PropertyChangeListener listener) {
            pcs.removePropertyChangeListener(listener);
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {

            int oldX = this.x;
            this.x = x;
            pcs.firePropertyChange("X", oldX, x);
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            int oldY = this.y;
            this.y = y;
            pcs.firePropertyChange("Y", oldY, y);
        }
    }

    public static class Client {

        public void logger(PropertyChangeEvent event) {
            System.out.println(event);
        }
    }

    public static void main(String[] args) {
        Client client = new Client();
        Point point = new Point();
        point.addPropertyChangeListener(client::logger);

        point.setX(1);
        point.setY(2);
    }
}
