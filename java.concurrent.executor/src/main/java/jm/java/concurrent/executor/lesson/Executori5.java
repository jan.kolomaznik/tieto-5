package jm.java.concurrent.executor.lesson;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class Executori5 {

    static Callable<String> callable(String result, long sleepSeconds) {
        return () -> {
            try {
                Thread.sleep(sleepSeconds * 1000);
                System.out.println(result + " DONE.");
            } catch (InterruptedException e) {
                System.out.println(result + " KILL.");
            }
            return result;
        };
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        ExecutorService executor = Executors.newCachedThreadPool();

        List<Callable<String>> callables = Arrays.asList(
                callable("task1", 2),
                callable("task2", 1),
                callable("task3", 3));

        String result = executor.invokeAny(callables);
        System.out.println(result);
    }
}
