package jm.java.concurrent.executor.lesson;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class Executori4 {

    public static void main(String[] args) throws ExecutionException, InterruptedException, TimeoutException {
        ExecutorService executor = Executors.newCachedThreadPool();


        List<Callable<String>> callables = Arrays.asList(
                () -> Thread.currentThread().getName(),
                () -> Thread.currentThread().getName(),
                () -> Thread.currentThread().getName(),
                () -> Thread.currentThread().getName(),
                () -> Thread.currentThread().getName(),
                () -> Thread.currentThread().getName(),
                () -> Thread.currentThread().getName(),
                () -> Thread.currentThread().getName());

        executor.invokeAll(callables)
                .stream()
                .map(future -> {
                    try {
                        return future.get();
                    }
                    catch (Exception e) {
                        throw new IllegalStateException(e);
                    }
                })
                .forEach(System.out::println);
    }
}
