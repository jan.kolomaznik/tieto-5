package jm.java.annotation.exercise;

public class Person {

    // TODO Create annotaion @NotNull
    //@NotNull(critical = true)
    private String name;

    // TODO Create annotaion @NotNull
    //@NotNull
    private String surname;

    private int age;

    public Person(String  name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public void setNameAndSurname(
            /*@NotNull*/ String name,
            /*@NotNull*/ String surname) {

    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                '}';
    }
}

