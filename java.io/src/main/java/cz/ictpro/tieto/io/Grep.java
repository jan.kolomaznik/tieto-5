package cz.ictpro.tieto.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Grep {

    private Pattern pattern;

    public static void main(String[] args) throws IOException {
        if (args.length != 2) {
            System.out.println("grep REGEX FILE");
            return;
        }
        Grep grep = new Grep(args[0]);
        Path path = Paths.get(args[1]);

        long startTime = System.currentTimeMillis();
        grep.grep(path)
                .count();
                //.forEach(System.out::println);
        long endTime = System.currentTimeMillis();
        System.out.format("Time: %d ms%n", (endTime - startTime));
    }

    public Grep(String regex) {
        this.pattern = Pattern.compile(regex);
    }

    public Stream<String> grep(Path path) throws IOException {
        return Files.walk(path)
                .parallel()
                .filter(Files::isRegularFile)
                .filter(Files::isReadable)
                .flatMap(this::grepFile);
    }

    private Stream<String> grepFile(Path path) {
        try (BufferedReader br = Files.newBufferedReader(path)) {
            List<String> lines = new LinkedList<>();
            String line;
            while ((line = br.readLine()) != null) {
                if (pattern.matcher(line).find()) {
                    lines.add(line);
                }
            }
            return lines.stream();
        } catch (Exception e) {
            //e.printStackTrace();
            return Stream.empty();
        }

    }

}
