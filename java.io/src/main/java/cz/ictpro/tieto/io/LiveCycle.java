package cz.ictpro.tieto.io;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class LiveCycle implements AutoCloseable {

    public static void main(String[] args) {
        // Stary zpusob
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("readme.md"));
            // Zpracování
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("MSG", e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    throw new IllegalArgumentException("MSG", e);
                }
            }
        }

        // Novy zpusob
        try (BufferedReader reader2 = new BufferedReader(new FileReader("readme.md"))) {
            // Zpracování
            reader2.lines()
                    .forEach(System.out::println);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("MSG", e);
        } catch (IOException e) {
            throw new IllegalArgumentException("MSG", e);
        }

        try (LiveCycle lc = new LiveCycle()) {

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void close() throws Exception {

    }
}
