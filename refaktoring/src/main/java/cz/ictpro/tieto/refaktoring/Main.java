package cz.ictpro.tieto.refaktoring;

import static cz.ictpro.tieto.refaktoring.Age.age;
import static cz.ictpro.tieto.refaktoring.FirstName.firstName;

public class Main {


    public static void main(String[] args) {
        new Person(
                firstName("Jan"),
                "Kolomaznik",
                age(10), // Age
                Gender.MALE);
    }
}
