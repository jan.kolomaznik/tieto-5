package cz.ictpro.tieto.refaktoring;

public enum Gender {

    MALE, FEMALE;
}
