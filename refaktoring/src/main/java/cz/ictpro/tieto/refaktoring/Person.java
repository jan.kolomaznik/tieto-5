package cz.ictpro.tieto.refaktoring;

public class Person {

    private FirstName firstName;
    private String lastName;

    private Age age;

    private Gender gender;

    public Person(FirstName firstName, String lastName, Age age, Gender gender) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
    }
}
