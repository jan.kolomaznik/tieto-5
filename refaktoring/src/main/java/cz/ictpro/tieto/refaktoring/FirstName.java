package cz.ictpro.tieto.refaktoring;

public class FirstName {

    public static FirstName firstName(String name) {
        return new FirstName(name);
    }

    private String name;

    private FirstName(String name) {
        this.name = name;
    }
}
