package cz.ictpro.tieto.nullObject;

import java.util.Optional;

public class Point {

    public static final Point NULL = new Point(0,0) {
        @Override
        public double getDistance(Point point) {
            throw  new IllegalStateException();
        }
    };

    private int x, y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public double getDistance(Point point) {
        return Math.sqrt(Math.pow(x - point.x, 2) + Math.pow(y - point.y, 2));
    }

    public Optional<Point> transform(String madParams) {
        return Optional.of(new Point(1,1));
    }
}
