package cz.ictpro.tieto.nullObject;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        Point p = new Point(1,2);
        List l = Collections.EMPTY_LIST;

        Optional<Point> tp = p.transform(":-)");
        if (tp.isPresent()) {
            System.out.println(tp.get());
        }

        tp.ifPresent(Main::nextOperation);

    }


    public static void nextOperation(Point point) {
        // todo
    }
}
