package cz.ictpro.tieto.servant;

public class RegistrationServant {

    public interface IServiced {

        void setId(String id);
    }

    public void registration(IServiced iServiced, String id) {
        iServiced.setId(id);
    }

    public void unregistration(IServiced iServiced) {
        iServiced.setId(null);
    }
}
