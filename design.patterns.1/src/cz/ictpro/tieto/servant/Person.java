package cz.ictpro.tieto.servant;

public class Person implements RegistrationServant.IServiced {

    private String id;

    private RegistrationServant registrationServant = new RegistrationServant();

    @Override
    public void setId(String id) {
        this.id = id.toUpperCase();
    }

    public void registration(String id) {
        registrationServant.registration(this, id);
    }

    public void unregistration() {
        registrationServant.unregistration(this);
    }
}
