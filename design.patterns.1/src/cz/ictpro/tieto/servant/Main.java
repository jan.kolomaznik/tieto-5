package cz.ictpro.tieto.servant;

public class Main {

    public static void main(String[] args) {
        Auto auto = new Auto();
        Person person = new Person();

        auto.registration("ID");
        person.registration("ID");

        auto.unregistration();
        person.unregistration();
    }
}
