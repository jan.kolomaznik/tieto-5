import java.awt.*;
import java.awt.event.*;
                
public class Zaklad extends Frame {
  Zaklad() {
    super.setTitle(getClass().getName());
    this.setLayout(new FlowLayout());

    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        System.exit(0);
      }
    });
  }
}
