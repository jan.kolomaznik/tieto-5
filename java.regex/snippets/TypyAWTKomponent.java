import java.awt.*;
import java.awt.event.*;

public class TypyAWTKomponent extends Zaklad {
  public TypyAWTKomponent() {
    super.setTitle(getClass().getName());
    this.setLayout(new BorderLayout(0, 0));
    this.setBackground(Color.lightGray);

    Panel flow1PN = new Panel();
//    flow1PN.setBackground(Color.cyan);
    flow1PN.setLayout(new FlowLayout(FlowLayout.CENTER, 20, 0));
    TextField textField = new TextField("TextField");
    flow1PN.add(textField);
    Button button = new Button("Button");
    flow1PN.add(button);

    Panel flow2PN = new Panel();
//    flow2PN.setBackground(Color.magenta);
    flow2PN.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
    Label chkbLB = new Label("CheckboxGroup", Label.CENTER);
    flow2PN.add(chkbLB);

    Panel flow3PN = new Panel();
//    flow3PN.setBackground(Color.orange);
    flow3PN.setLayout(new FlowLayout(FlowLayout.CENTER, 10, -5));
    CheckboxGroup cbg = new CheckboxGroup();
    flow3PN.add(new Checkbox("a", cbg, true));
    flow3PN.add(new Checkbox("b", cbg, false));
    flow3PN.add(new Checkbox("c", cbg, false));

    Panel flow4PN = new Panel();
//    flow4PN.setBackground(Color.yellow);
    flow4PN.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
    Checkbox prvniChB = new Checkbox("Checkbox 1", true);
    Checkbox druhyChB = new Checkbox("Checkbox 2", true);
    flow4PN.add(prvniChB);
    flow4PN.add(druhyChB);

    Panel flow5PN = new Panel();
//    flow5PN.setBackground(Color.pink);
    flow5PN.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 5));
    Label label = new Label("Label", Label.CENTER);
    flow5PN.add(label);

    Choice choice = new Choice();
    choice.add("Choice - polozka 1");
    choice.add("Choice - polozka 2");
    flow5PN.add(choice);

    Panel gridPN = new Panel();
//    gridPN.setBackground(Color.green);
    gridPN.setLayout(new GridLayout(5, 1));
    gridPN.add(flow1PN);
    gridPN.add(flow2PN);
    gridPN.add(flow3PN);
    gridPN.add(flow4PN);
    gridPN.add(flow5PN);

    Panel bor1PN = new Panel();
//    bor1PN.setBackground(Color.blue);
    bor1PN.setLayout(new BorderLayout(0, 15));
    bor1PN.add(gridPN, BorderLayout.CENTER);

    Scrollbar vert = new Scrollbar(Scrollbar.VERTICAL);
    vert.setBackground(Color.white);
    bor1PN.add(vert, BorderLayout.EAST);

    Scrollbar horiz = new Scrollbar(Scrollbar.HORIZONTAL);
    horiz.setBackground(Color.white);
    bor1PN.add(horiz, BorderLayout.NORTH);

    Panel bor2PN = new Panel();
//    bor2PN.setBackground(Color.red);
    bor2PN.setLayout(new BorderLayout(10, 10));
    bor2PN.add(bor1PN, BorderLayout.CENTER);

    List list = new List(12);
    for (int i = 1;  i < 30;  i++)
      list.add("List - polozka " + i);
    bor2PN.add(list, BorderLayout.EAST);

    this.add(bor2PN, BorderLayout.CENTER);
    this.add(new Label("   ScrollBar", Label.CENTER),
             BorderLayout.NORTH);
    this.pack();
  }

  public Insets getInsets() {
    return new Insets(25, 20, 20, 20);
  }

  public static void main(String[] args) {
    new TypyAWTKomponent().setVisible(true);
  }
}
