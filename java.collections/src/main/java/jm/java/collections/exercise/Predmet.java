package jm.java.collections.exercise;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Predmet {

    private final String kod;

    private final List<Cviceni> cviceniList = new ArrayList<>();

    private final SortedSet<Student> studentSortedSet = new TreeSet<>();
   
    public Predmet(String kod) {
        this.kod = kod;
    }

    public Cviceni zalozCviceni(int kapacita) {
        Cviceni cviceni = new Cviceni(kapacita, this);
        cviceniList.add(cviceni);
        return cviceni;
    }


    public Collection<Cviceni> getCvicenis() {
        return Collections.unmodifiableCollection(cviceniList);
    }


    public Cviceni getCviceni(int index) {
        return cviceniList.get(index);
    }

    public boolean zapisDoPredmetu(Student student) {
        return studentSortedSet.add(student);
    }

    public Collection<Student> getStudents() {
        return Collections.unmodifiableCollection(studentSortedSet);
    }

    public Collection<Student> getStudentyBezCviceni() {
        /*
        Set<Student> studentiVeCviceni = new HashSet<>();
        for (Cviceni cviceni: cviceniList) {
            studentiVeCviceni.addAll(cviceni.getStudents());
        }
        Set<Student> result = new HashSet<>(studentSortedSet);
        result.removeAll(studentiVeCviceni);
        return result;
        */
        Set<Student> studentiVeCviceni = cviceniList.stream()
                .flatMap(c -> c.getStudents().stream())
                .collect(Collectors.toSet());
        return studentSortedSet.stream()
                .filter(s -> !studentiVeCviceni.contains(s))
                .collect(Collectors.toSet());
        /*
        return studentSortedSet.stream()
                .filter(s -> cviceniList.stream()
                        .flatMap(c -> c.getStudents().stream())
                        .noneMatch(student -> s.equals(student)))
                .collect(Collectors.toSet());
        */
    }

    public String getKod() {
        return kod;
    }

    public int getKapacita() {
        /*
        int result = 0;
        for (Cviceni cviceni: cviceniList) {
            result += cviceni.getKapacita();
        }
        return result;
        */
        return cviceniList.stream()
                .mapToInt(Cviceni::getKapacita)
                .sum();
    }

    @Override
    public String toString() {
        return "Predmet{" +
                "kod='" + kod + '\'' +
                ", cvicenis=" + cviceniList +
                ", students=" + studentSortedSet +
                '}';
    }
}
