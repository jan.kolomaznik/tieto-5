package jm.java.collections.exercise;

import java.util.Comparator;
import java.util.Objects;

/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Student implements Comparable<Student> {

    public static final Comparator<Student> COMPARE_BY_NAME = new Comparator<Student>() {
        @Override
        public int compare(Student o1, Student o2) {
            if (!Objects.equals(o1.prijmeni, o2.prijmeni)) {
                return o1.prijmeni.compareTo(o2.prijmeni);
            }
            return o1.jmeno.compareTo(o2.jmeno);
        }
    };

    private final String jmeno;
    private final String prijmeni;

    public Student(String jmeno, String prijmeni) {
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(jmeno, student.jmeno) &&
                Objects.equals(prijmeni, student.prijmeni);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jmeno, prijmeni);
    }

    @Override
    public String toString() {
        return "'" + jmeno + ' ' + prijmeni + "'";
    }

    @Override
    public int compareTo(Student o) {
        if (!Objects.equals(prijmeni, o.prijmeni)) {
            return prijmeni.compareTo(o.prijmeni);
        }

        return jmeno.compareTo(o.jmeno);
    }
}
