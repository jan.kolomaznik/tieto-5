package jm.java.collections.exercise;


import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by xkoloma1 on 02.11.2016.
 */
public class Cviceni {

    private final int kapacita;

    private final Predmet predmet;

    private final Set<Student> studentSet = new HashSet<>();

    public Cviceni(int kapacita) {
        this(kapacita, null);
    }

    public Cviceni(int kapacita, Predmet predmet) {
        this.kapacita = kapacita;
        this.predmet = predmet;
    }

    public boolean prihlasit(Student student) {
        if (studentSet.size() >= kapacita) {
            return false;
        }
        if (predmet != null && !predmet.getStudentyBezCviceni().contains(student)) {
            return false;
        }
        return studentSet.add(student);
    }

    @Override
    public String toString() {
        return "Cviceni{" +
                "kapacita=" + kapacita +
                ", studentSet=" + studentSet +
                '}';
    }

    public int getKapacita() {
        return kapacita;
    }

    public Collection<Student> getStudents() {
        return Collections.unmodifiableCollection(studentSet);
    }


}
