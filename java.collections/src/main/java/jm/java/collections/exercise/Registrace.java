package jm.java.collections.exercise;

import java.util.Collections;
import java.util.LinkedList;
import java.util.Queue;

public class Registrace {

    private Predmet predmet;

    private Queue<Student> studentQueue = new LinkedList<>();

    public Registrace(Predmet predmet) {
        this.predmet = predmet;
    }

    public void zaregistruj(Student student) {
        if (studentQueue.contains(student))
            return;

        studentQueue.add(student);
    }

    public Queue<Student> getStudents() {
        return studentQueue;
    }


    public int hromadnyZapis() {
        int kapacitaPredmetu = predmet.getKapacita();
        int zapsatStudnetu = Math.min(studentQueue.size(), kapacitaPredmetu);
        for (int i = 0; i < zapsatStudnetu; i++) {
            predmet.zapisDoPredmetu(studentQueue.remove());
        }
        return studentQueue.isEmpty()
                ? kapacitaPredmetu - zapsatStudnetu
                : - studentQueue.size();
    }

    @Override
    public String toString() {
        return "Registrace{" +
                "predmet=" + predmet.getKod() +
                ", studentQueue=" + studentQueue +
                '}';
    }
}
