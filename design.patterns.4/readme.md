[remark]:<class>(center, middle)
# Přehlednávrhových vzorů:  
----
*Optimalizace rozhraní*

[remark]:<slide>(new)
## Facade

Návrhový vzor Facade představuje řešení, jestliže je nutné zjednodušit vstupní bod do systému.

[remark]:<slide>(wait)
![](media/Facade.svg)

[remark]:<slide>(new)
### Příklad implementace
Create an interface.

```java
public interface Shape {
   void draw();
}
```

[remark]:<slide>(wait)
Create concrete classes implementing the same interface.

```java
public class Rectangle implements Shape {

   @Override
   public void draw() { System.out.println("Rectangle::draw()"); }
}
```

```java
public class Square implements Shape {

   @Override
   public void draw() { System.out.println("Square::draw()"); }
}
```

```java
public class Circle implements Shape {

   @Override
   public void draw() { System.out.println("Circle::draw()"); }
}
```

[remark]:<slide>(new)
Create a facade class.

```java
public class ShapeMaker {
   private Shape circle;
   private Shape rectangle;
   private Shape square;

   public ShapeMaker() {
      circle = new Circle();
      rectangle = new Rectangle();
      square = new Square();
   }

   public void drawCircle(){
      circle.draw();
   }
   public void drawRectangle(){
      rectangle.draw();
   }
   public void drawSquare(){
      square.draw();
   }
}
```

[remark]:<slide>(new)
Use the facade to draw various types of shapes.

```java
public class FacadePatternDemo {
   public static void main(String[] args) {
      ShapeMaker shapeMaker = new ShapeMaker();

      shapeMaker.drawCircle();
      shapeMaker.drawRectangle();
      shapeMaker.drawSquare();		
   }
}
```

[remark]:<slide>(new)
## Adapter
Tento vzor použijeme ve chvíli, když potřebujeme, aby třída mněla jiné rozhraní než to, které právě má.
 
[remark]:<slide>(wait) 
![](media/Adapter_1.svg)

[remark]:<slide>(new)
### Příklad implementace
Create interfaces for Media Player and Advanced Media Player.

```java
public interface MediaPlayer {
   public void play(String audioType, String fileName);
}
```

```java
public interface AdvancedMediaPlayer {	
   public void playVlc(String fileName);
   public void playMp4(String fileName);
}
```

[remark]:<slide>(new)
Create concrete classes implementing the AdvancedMediaPlayer interface.

```java
public class VlcPlayer implements AdvancedMediaPlayer{
   @Override
   public void playVlc(String fileName) {
      System.out.println("Playing vlc file. Name: "+ fileName);		
   }

   @Override
   public void playMp4(String fileName) {
      //do nothing
   }
}
```

```java
public class Mp4Player implements AdvancedMediaPlayer{

   @Override
   public void playVlc(String fileName) {
      //do nothing
   }

   @Override
   public void playMp4(String fileName) {
      System.out.println("Playing mp4 file. Name: "+ fileName);
   }
}
```

[remark]:<slide>(new)
Create adapter class implementing the MediaPlayer interface.

```java
public class MediaAdapter implements MediaPlayer {

   AdvancedMediaPlayer advancedMusicPlayer;

   public MediaAdapter(String audioType){
   
      if(audioType.equalsIgnoreCase("vlc") ){
         advancedMusicPlayer = new VlcPlayer();
         
      }else if (audioType.equalsIgnoreCase("mp4")){
         advancedMusicPlayer = new Mp4Player();
      }
   }

   @Override
   public void play(String audioType, String fileName) {
   
      if(audioType.equalsIgnoreCase("vlc")){
         advancedMusicPlayer.playVlc(fileName);
      }
      else if(audioType.equalsIgnoreCase("mp4")){
         advancedMusicPlayer.playMp4(fileName);
      }
   }
}
```

[remark]:<slide>(new)
Create concrete class implementing the MediaPlayer interface.

```java
public class AudioPlayer implements MediaPlayer {
   MediaAdapter mediaAdapter; 

   @Override
   public void play(String audioType, String fileName) {

      //inbuilt support to play mp3 music files
      if(audioType.equalsIgnoreCase("mp3")){
         System.out.println("Playing mp3 file. Name: " + fileName);
      } 
      
      //mediaAdapter is providing support to play other file formats
      else if(audioType.equalsIgnoreCase("vlc") 
           || audioType.equalsIgnoreCase("mp4")){
         mediaAdapter = new MediaAdapter(audioType);
         mediaAdapter.play(audioType, fileName);
      }
      
      else{
         System.out.println("Invalid media: " + audioType);
      }
   }   
}
```

[remark]:<slide>(new)
Use the AudioPlayer to play different types of audio formats.

```java
public class AdapterPatternDemo {
   public static void main(String[] args) {
      AudioPlayer audioPlayer = new AudioPlayer();

      audioPlayer.play("mp3", "beyond the horizon.mp3");
      audioPlayer.play("mp4", "alone.mp4");
      audioPlayer.play("vlc", "far far away.vlc");
      audioPlayer.play("avi", "mind me.avi");
   }
}
```

[remark]:<slide>(new)
## Composite
Návrhový vzor Composite představuje řešení, jak uspořádat jednoduché objekty a z nich složené (kompozitní) objekty. 

Snahou vzoru je, aby k oběma typům objektů (jednoduchým a složeným) bylo možné přistoupit jednotným způsobem.

[remark]:<slide>(wait)
![](media/Composite.svg)

[remark]:<slide>(new)
### Příklad implementace
Create Employee class having list of Employee objects.

```java
public class Employee {
   private String name;
   private String dept;
   private int salary;
   private List<Employee> subordinates;

   public Employee(String name,String dept, int sal) {
      this.name = name;
      this.dept = dept;
      this.salary = sal;
      subordinates = new ArrayList<Employee>();
   }

   public void add(Employee e) { subordinates.add(e); }

   public void remove(Employee e) { subordinates.remove(e); }

   public List<Employee> getSubordinates(){ return subordinates; }

   public String toString(){
      return ("Employee :[ Name : " + name + ", " +
                          "dept : " + dept + ", " +
                        "salary : " + salary +"]");
   }   
}
```

[remark]:<slide>(new)
Use the Employee class to create and print employee hierarchy.

```java
public class CompositePatternDemo {
   public static void main(String[] args) {
   
      Employee CEO = new Employee("John","CEO", 30000);
      Employee headSales = new Employee("Robert","Head Sales", 20000);
      Employee headMarketing = new Employee("Michel","Head Marketing", 20000);
      Employee clerk1 = new Employee("Laura","Marketing", 10000);
      Employee clerk2 = new Employee("Bob","Marketing", 10000);
      Employee salesExecutive1 = new Employee("Richard","Sales", 10000);
      Employee salesExecutive2 = new Employee("Rob","Sales", 10000);

      CEO.add(headSales);
      CEO.add(headMarketing);

      headSales.add(salesExecutive1);
      headSales.add(salesExecutive2);
      headMarketing.add(clerk1);
      headMarketing.add(clerk2);

      //print all employees of the organization
      System.out.println(CEO); 
      for (Employee headEmployee : CEO.getSubordinates()) {
         System.out.println(headEmployee);
         for (Employee employee : headEmployee.getSubordinates()) {
            System.out.println(employee);
         }
      }
   }
}
```
