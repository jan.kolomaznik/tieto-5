[remark]:<class>(center, middle)

Paralelní programování
======================
Semafor
----

[remark]:<slide>(new)
## Semafor

* programová struktura, která se hodí pro synchronizaci
  * strukturované, přenosné, univerzální
  
* alternativy: komplexnější struktury
* semafor = integer
* při vytvoření se nastaví počáteční hodnota
* při průchodu se sníží o 1
* pokud je hodnota záporná pak se vlákno uspí (will block)
* pokud se hodnota semaforu zvýší na 0, pak je některé ze spících vláken probuzeno (wake-up)

[remark]:<slide>(new)
## Semafor

* u standardního semaforu nevíme, které vlákno bude probuzeno
* může existovat „férový“ semafor, který probudí nejstarší vlákno
  * zabraňuje stárnutí
  * u pracovních vláken není moc žádoucí

* aktuální hodnotu semaforu nelze zjistit – stav semaforu je „neurčitý“
* semafor je atomický
  * vlákno buď projde nebo neprojde

* hodnota semaforu = počet propustek
* binární semafor = jedna propustka = mutex

[remark]:<slide>(new)
## Semafor

* průchod semaforem
  * wait() = acquire() = decrement()
  * signal() = release() = increment()

* existují různá rozšíření:
  * fair semaphore
  * tryacquire()

* semafory se kombinují do složitějších struktur
  * experiment je k ničemu
  * synchronizační vzory

[remark]:<slide>(new)
## Semaphore

* Javě se pro práci se semaforem používají metody:
    * `acquire()`
    * `release()`

```java
Semaphore semaphore = new Semaphore(1);

//critical section
semaphore.acquire();
...
semaphore.release();
```

* Rozšiřující metody:
    * `availablePermits()`
    * `acquireUninterruptibly()`
    * `drainPermits()`
    * `hasQueuedThreads()`
    * `getQueuedThreads()`
    * `tryAcquire()`
