package cz.ictpro.tieto.concurency;

import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class Entity {

    private static AtomicLong nextid = new AtomicLong();

    private final Long id;

    public Entity() {
        this.id = nextid.incrementAndGet();
    }

    public static void main(String[] args) {
        final Set<Long> ids = Collections.synchronizedSet(new HashSet<>());
        while (true) {
            new Thread() {
                @Override
                public void run() {
                    Entity entity = new Entity();
                    if (!ids.add(entity.id)) {
                        System.out.println("Bingo: " + entity.id);
                        System.exit(1);
                    }
                }
            }.start();
        }
    }
}
