package jm.lombok.lesson;

import lombok.Data;

import java.time.LocalDate;
import java.util.Objects;

@Data
public class Person extends Object {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        System.out.println(new Person().toString());
    }
}
