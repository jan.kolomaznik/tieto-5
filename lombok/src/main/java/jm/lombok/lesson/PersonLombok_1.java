package jm.lombok.lesson;

import lombok.*;

import java.time.LocalDate;

@Data
public class PersonLombok_1 {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        new PersonLombok_1();
        //new PersonLombok_1("Kumar", null, LocalDate.now());
    }
}
