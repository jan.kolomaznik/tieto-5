package jm.lombok.lesson;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@AllArgsConstructor
@Getter @Setter
public class PersonLombok_2 {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        PersonLombok_2 person = new PersonLombok_2("Pepa", "Zdepa", LocalDate.now());
        person.setFirstName("Tomas");
        System.out.println("firstName: " + person.getFirstName());
        System.out.println("lastName: " + person.getLastName());
    }
}

