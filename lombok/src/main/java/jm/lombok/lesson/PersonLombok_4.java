package jm.lombok.lesson;

import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.experimental.Wither;

import java.time.LocalDate;

@Value
@Builder
@Wither
public class PersonLombok_4 {

    @NonNull
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {

            PersonLombok_4.builder()
                    //.firstName("Neco")
                    .lastName("nekdo")
                    .build();


    }
}

