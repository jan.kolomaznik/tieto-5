[remark]:<class>(center, middle)
# Projekt Lombok

[remark]:<slide>(new)
## Motivace

Jedním z problémů, kterým trpí Java je poměrně větší množství kódu.

Java má velmi málo programových konstrukcí, což je na jedné straně výhoda.
- Nemá řádné property
- Přetěžování operátorů
- Zkrácené zápisy a vestavěné funkce ...

Takže se velmi snadno po této stránce učí.

[remark]:<slide>(wait)
Na druhé straně množství věcí musíte napsat v kódu.

Tento problém trochu řeší různé generátory, ale mějme například jednoduchý POJO objekt pro osobu.

Ta by mohla mít tři atributy: jmeno, příjmení a datum narození ...

```java
public class Person {

   private String firstName;
   private String lastName;
   private LocalDate dateOfBirth;

    // TODO zkusite do-generovat
    
   public Person(String firstName, String lastName, LocalDate dateOfBirth) {
      super();
      this.firstName = firstName;
      this.lastName = lastName;
      this.dateOfBirth = dateOfBirth;
   }

   public String getFirstName() {
      return firstName;
   }

   public void setFirstName(String firstName) {
      this.firstName = firstName;
   }

   public String getLastName() {
      return lastName;
   }

   public void setLastName(String lastName) {
      this.lastName = lastName;
   }

   public LocalDate getDateOfBirth() {
      return dateOfBirth;
   }

   public void setDateOfBirth(LocalDate dateOfBirth) {
      this.dateOfBirth = dateOfBirth;
   }

   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
      result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
      result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
      return result;
   }

   @Override
   public boolean equals(Object obj) {
      if (this == obj)
         return true;
      if (obj == null)
         return false;
      if (getClass() != obj.getClass())
         return false;
      Person other = (Person) obj;
      if (dateOfBirth == null) {
         if (other.dateOfBirth != null)
            return false;
      } else if (!dateOfBirth.equals(other.dateOfBirth))
         return false;
      if (firstName == null) {
         if (other.firstName != null)
            return false;
      } else if (!firstName.equals(other.firstName))
         return false;
      if (lastName == null) {
         if (other.lastName != null)
            return false;
      } else if (!lastName.equals(other.lastName))
         return false;
      return true;
   }

   @Override
   public String toString() {
      return "Person [firstName=" + firstName + ", lastName=" + lastName + "dateOfBirth=" + dateOfBirth + "]";
   }

}
```

[remark]:<slide>(new)
## Motivace
Třída by měla být zapouzdřená a měla mi obsahovat:
 
- Konstruktor
- Getter pro každou vlastnost
- Setter pro každou vlastnost
- Metody hashCode a equals
- Metodu toString

[remark]:<slide>(wait)
Tedy kolem 80+ řádků kódu.

[remark]:<slide>(wait)
A žádnou byznys logiku, řádný funkční kód ...


[remark]:<slide>(new)
## Project Lombok
Projekt Lombok je Java knihovna, která se integruje od editoru a kompilátoru a poskytuje nástroje pro redukci přebytečného kódu.

Stránky projektu: https://projectlombok.org/

Doporučuji shlédnout video, je tam defacto všechno ;-)

[remark]:<slide>(wait)
### Jak funguje Lombok?

Lombok pomocí @Annotací rozšiřuje kód v době v rámci kompilace.
 
Lombok tedy redukuje kód v okamžiku zobrazení v editoru.
 
Pomáhá tak udržovat kód malý, čistý a snadno čitelný a udržovatelný.

[remark]:<slide>(new)
### Jak jej přidám do projektu?
Přidání Lomboku do projektu je jednoduché. 

Stačí přidat níže uvedenou závislost v souboru pom.xml projektu maven.

```xml
<dependency>
   <groupId>org.projectlombok</groupId>
   <artifactId>lombok</artifactId>
   <version>1.18.2</version>
</dependency>
```

nebo v Gradle
```groovy
dependencies {
    compileOnly 'org.projectlombok:lombok:1.18.2'
}
```

[remark]:<slide>(new)
### Instalace pluginu do JIdea 

Přidejte Lombok Plugin v IDE

![](media/instal-plugin.png)

[remark]:<slide>(new)
A povolit annotation prosesing

![](media/annotation-compiler.png)

[remark]:<slide>(new)
## Project Lombok Annotations
Projekt Lombok poskytuje mnoho anotací, které pomáhají snížit množství kódu. 

Následně se seznámíme s některými z nich.

Obecně ale doporučuji to s Lombok anotacemi "nepřehánět" a používat je spíše úsporně.

[remark]:<slide>(new)
### Constructor Annotation

```java
@AllArgsConstructor
public class PersonLombok_1 {
    
    @NonNull 
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    
    public static void main(String[] args) {
        new PersonLombok(null, "Zdepa", LocalDate.now());
    }
}
```

[remark]:<slide>(wait)
#### Rozbor příkladu:
Konstruktor s argumenty: `@AllArgsConstructor`

Nulová kontrola během předání argumentu v konstruktoru pomocí `@NonNull`. 

`@NonNull` se také použít také při předávání argumentu jako parametru metody

[remark]:<slide>(new)
### Getter/Setter Annotations

Tyto anotace lze použít buď na úrovni pole nebo třídy. 

Při použití na úrovni třídy se vytvářejí všechny getters a setters metody. 

[remark]:<slide>(wait)
```java
@AllArgsConstructor @Getter @Setter
public class PersonLombok_2 {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        PersonLombok_2 person = new PersonLombok_2("Pepa", "Zdepa", null);
        person.setFirstName("Tomas");
        System.out.println("firstName: " + person.getFirstName());
        System.out.println("lastName: " + person.getLastName());
    }
}
```

[remark]:<slide>(new)
### equals, hashCode and toString annotations
Doporučuje se při vytváření třídy přepsat metody `hashCode()` a `equals()`. 

V Lomboku máme anotaci `@EqualsAndHashCode`, která tyto metody vygeneruje.

Dále anotace `@ToString` poskytuje implementaci metody `toString()`. 

[remark]:<slide>(wait)
```java
@AllArgsConstructor @Getter @Setter
@EqualsAndHashCode 
@ToString
public class PersonLombok_3 {
    
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    
    public static void main(String[] args) {
        PersonLombok_3 person = new PersonLombok_3("Pepa", "Zdepa", null);
        System.out.println(person);
        System.out.println("Equals: " + person.equals(
                new PersonLombok_3("Pepa", "Zdepa", LocalDate.now())
        ));
    }
}
```
 
[remark]:<slide>(new)
### annotace @Data/@Value
Nyní máme třídu osob bez jakéhokoliv generovaného kódu.
 
Nicméně k dispozici jsou ještě dvě annotace, které předchozí annotace seskupují:

[remark]:<slide>(wait)
**@Data**: pro vytvoření datové POJO třídy

```java
@Data
public class PersonLombok_3 {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        PersonLombok_3 person = new PersonLombok_3();
        person.setFirstName("Pepa");
        person.setLastName("Zeepa");
        System.out.println(person);
        System.out.println("Equals: " + person.equals(
                new PersonLombok_3()
        ));
    }
}
```

[remark]:<slide>(new)
**@Value**: pro vytvoření immutable třídy

```java
@Value
public class PersonLombok_4 {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        PersonLombok_4 person = new PersonLombok_4("Pepa", "Zdepa", null);
        System.out.println(person);
    }
}
```

[remark]:<slide>(new)
### Design Pattern based Annotations
Lombok obsahuje anotace pro některé návrhové vzory

[remark]:<slide>(wait)
**@Builder**: doplní do třídy kód implementující návrhový vzor:

```java
@Builder
public class Employee {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        Employee person = Employee.builder()
                .firstName("Pepa")
                .lastName("Zdepa")
                .build();
        System.out.println(person);
    }
}
```

[remark]:<slide>(new)
**@Delegate**: generuje metody delegátů.

Tento návrhový vzor nahrazuje dědičnost, ale vytváří velké množství spoustu kódu.

```java
@RequiredArgsConstructor
public class AdapterImpl implements Adapter {
    @Delegate
    private final Adaptee instance;

    public static void main(String[] args) {
        AdapterImpl impl = new AdapterImpl(new Adaptee());
        impl.display();
    }
}

interface Adapter {
    public void display();
}

class Adaptee {
    public void display() {
        System.out.println("In Adaptee.display()");
    }
}
```

[remark]:<slide>(new)
### Imutable s with metodami
U imutable class se vygenerují metody pro "změnu"

```java
@Value @Wither
public class ImmutableEmployee {

    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

    public static void main(String[] args) {
        ImmutableEmployee person = new ImmutableEmployee("Pepa", "Zdepa", null);
        System.out.println(person.withDateOfBirth(LocalDate.now()));
        System.out.println(person);

    }
}
```

[remark]:<slide>(new)
### Loggery
Pro snažíš inicializaci loggeru ve třídě:

```java
@Log
public class LogEmployee {

    public void work(){
        log.info("Log method");
    }

    public static void main(String[] args) {
        LogEmployee employee = new LogEmployee();
        employee.work();
    }
}
```

[remark]:<slide>(new)
## Další informace:

Lombok má dvě kategorie annotací:
- **Stable**: https://projectlombok.org/features/all
- **Experimental**: https://projectlombok.org/features/experimental/all
