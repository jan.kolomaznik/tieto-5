package com.example.demo;

import org.springframework.stereotype.Service;

@Service
public class MyService {

    public void hello(String name) {
        System.out.format("Hello %s.%n", name);
    }
}
