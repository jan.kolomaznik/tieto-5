package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;

@SpringBootApplication
public class DemoApplication {

    @Autowired
    private ApplicationContext context;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @Bean
    public CommandLineRunner runner(FooService fooService) {
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
                fooService.method("Honza");
                Arrays.stream(context.getBeanDefinitionNames())
                        .forEach(System.out::println);
                MyService ms = context.getBean("myService", MyService.class);
                ms.hello("Tieto");
            }
        };
    }

}
