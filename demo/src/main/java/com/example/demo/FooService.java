package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FooService {

    @Autowired
    private MyService myService;

    public void method(String name) {
        myService.hello(name.toUpperCase());
    }
}
