package jm.java.functions.lesson;

public interface Point {

    double getX();
    double getY();

    default double distance(Point p) {
        return Math.sqrt(
                Math.pow(getX() - p.getX(), 2) +
                Math.pow(getY() - p.getY(), 2));
    }

}
