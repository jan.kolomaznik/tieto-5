package jm.java.functions.lesson;

public class DemoThis {

    private int cislo = 1;

    private class InnerClass {

        private int cislo = 2;

        public void foo() {
            System.out.println(DemoThis.this.cislo);
        }
    }

}
