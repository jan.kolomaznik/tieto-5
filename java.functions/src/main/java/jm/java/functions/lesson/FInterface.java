package jm.java.functions.lesson;

//@FunctionalInterface
public interface FInterface {

    void todo();

    default void doubleTodo() {
        todo();
        todo();
    }

}
